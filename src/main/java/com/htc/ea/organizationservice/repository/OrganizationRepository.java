package com.htc.ea.organizationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htc.ea.organizationservice.model.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {

}
