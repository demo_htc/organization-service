package com.htc.ea.organizationservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.organizationservice.client.fallback.ApiClientFallback;
import com.htc.ea.organizationservice.model.Department;
import com.htc.ea.organizationservice.model.Employee;


/**
 * Interfaz de consumo de APIs mediante el Gateway de Spring
 * Posee implementacion de la interfaz la cual captura los errores de conexion
 * @author htc
 *
 */
@FeignClient(name = "spring-gateway-service" , fallbackFactory = ApiClientFallback.class)
public interface ApiClient {
	
	/**
	 * consulta al microservicio department
	 * retorna lista de departamentos bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de department en el body
	 */
	@GetMapping(value="/departments/organizations/{id}")
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganization(@PathVariable("id") Long id);
	
	/**
	 * consulta al microservicio department
	 * retorna lista de departamentos con empleados bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de department en el body
	 */
	@GetMapping(value="/departments/organizations/{id}/employees")
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganizationWithEmployees(@PathVariable("id") Long id);
	
	/**
	 * consulta al microservicio employee
	 * retorna lista de empleados bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de empleados en el body
	 */
	@GetMapping("/employees/organizations/{id}")
	public ResponseEntity<List<Employee>> findAllEmployeesByIdOrganization(@PathVariable("id") Long id); 
}
