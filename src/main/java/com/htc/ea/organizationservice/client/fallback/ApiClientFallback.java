package com.htc.ea.organizationservice.client.fallback;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.organizationservice.client.ApiClient;
import com.htc.ea.organizationservice.model.Department;
import com.htc.ea.organizationservice.model.Employee;
import com.htc.ea.organizationservice.util.AppConst;

import feign.hystrix.FallbackFactory;

@Component
public class ApiClientFallback implements FallbackFactory<ApiClient>{


	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public ApiClient create(Throwable cause) {

		log.error("Feign error: {}",cause);
		
		return new ApiClient() {
			
			@Override
			public ResponseEntity<List<Employee>> findAllEmployeesByIdOrganization(Long id) {
				log.error("Entrada a metodo de respaldo: findAllEmployeesByIdOrganization");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Employee");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
			
			@Override
			public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganizationWithEmployees(Long id) {
				log.error("Entrada a metodo de respaldo: findAllDepartmentsByIdOrganizationWithEmployees");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
			
			@Override
			public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganization(Long id) {
				log.error("Entrada a metodo de respaldo: findAllDepartmentsByIdOrganization");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Department");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
		};
	}

}
