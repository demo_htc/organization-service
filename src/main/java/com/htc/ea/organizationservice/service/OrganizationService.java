package com.htc.ea.organizationservice.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.htc.ea.organizationservice.client.ApiClient;
import com.htc.ea.organizationservice.model.Department;
import com.htc.ea.organizationservice.model.Employee;
import com.htc.ea.organizationservice.model.Organization;
import com.htc.ea.organizationservice.repository.OrganizationRepository;
import com.htc.ea.organizationservice.util.AppConst;

@Service
public class OrganizationService {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private ApiClient apiClient;
	
	/**
	 * Funcion de guardado
	 * Guarda una organizacion en la base de datos
	 * si ocurre algun error lo maneja en el try catch
	 * @param organization
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio algun error
	 */
	public ResponseEntity<Organization> save(Organization organization){
		//datos que iran en response entity
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Organization result = null;
		try {
			//guardar organization
			result = organizationRepository.saveAndFlush(organization);
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION, "guardado correctamente");
			log.info("organizacion guardada correctamente");
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar guardar organizacion");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	
	/**
	 * funcion de busqueda de todos las organizaciones
	 * evalua si la lista esta vacia y tambien si ocurre algun error lo maneja con try-catch
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Organization>> findAll(){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Organization> result = null;
		//try catch para manejar exceptions que se puedan producir
		try {
			result = organizationRepository.findAll();
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Organizaciones encontradas : "+result.size());
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar organizaciones");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de por id
	 * busca una organization dado el id ingresado
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro
	 */
	public ResponseEntity<Organization> findById(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Organization result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Organization> searched = organizationRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = searched.get();
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_FOUND);
				log.info(AppConst.MSG_ORGANIZATION_FOUND);
			}else {
				headers.add(AppConst.MSG_DESCRIPTION,AppConst.MSG_ORGANIZATION_NOT_FOUND+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, AppConst.MSG_ORGANIZATION_ERROR_FOUND+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de por id
	 * busca una organization dado el id ingresado
	 * busca departamentos que esten en la organizacion
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro
	 */
	public ResponseEntity<Organization> findByIdWithDepartments(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Organization result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Organization> searched = organizationRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = searched.get();
				log.info(AppConst.MSG_ORGANIZATION_FOUND);
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_FOUND);
				ResponseEntity<List<Department>> resultDepartment = apiClient.findAllDepartmentsByIdOrganization(id);
				if(resultDepartment.getStatusCode().equals(HttpStatus.OK)) {
					result.setDepartments( resultDepartment.getBody() );
					status = HttpStatus.OK;
					headers.add(AppConst.MSG_DESCRIPTION, "departamentos agregados");
					log.info("departamentos agregados");
				}else {
					status = HttpStatus.BAD_GATEWAY;
					headers.addAll(resultDepartment.getHeaders());
				}
			}else {
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_NOT_FOUND+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, AppConst.MSG_ORGANIZATION_ERROR_FOUND+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	
	/**
	 * funcion de busqueda de por id
	 * busca una organization dado el id ingresado
	 * busca departamentos asignados a la organizacion
	 * busca empleados asignados a cada departamento
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro
	 */
	public ResponseEntity<Organization> findByIdWithDepartmentsWithEmployees(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Organization result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Organization> searched = organizationRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = searched.get();
				log.info(AppConst.MSG_ORGANIZATION_FOUND);
				headers.add(AppConst.MSG_DESCRIPTION,AppConst.MSG_ORGANIZATION_FOUND);
				ResponseEntity<List<Department>> resultDepartment = apiClient.findAllDepartmentsByIdOrganizationWithEmployees(id);
				if(resultDepartment.getStatusCode().equals(HttpStatus.OK)) {
					result.setDepartments( resultDepartment.getBody() );
					status = HttpStatus.OK;
					headers.add(AppConst.MSG_DESCRIPTION, "departamentos con empleados agregados");
					log.info("departamentos con empleados agregados");
				}else {
					status = HttpStatus.BAD_GATEWAY;
					headers.addAll(resultDepartment.getHeaders());
				}
			}else {
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_NOT_FOUND+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, AppConst.MSG_ORGANIZATION_ERROR_FOUND+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de por id
	 * busca una organization dado el id ingresado
	 * busca departamentos asignados a la organizacion
	 * busca empleados asignados a cada departamento
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro
	 */
	public ResponseEntity<Organization> findByIdWithEmployees(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Organization result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Organization> searched = organizationRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = searched.get();
				log.info(AppConst.MSG_ORGANIZATION_FOUND);
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_FOUND);
				ResponseEntity<List<Employee>> resultEmployee = apiClient.findAllEmployeesByIdOrganization(id);
				if(resultEmployee.getStatusCode().equals(HttpStatus.OK)) {
					result.setEmployees(resultEmployee.getBody());
					status = HttpStatus.OK;
					headers.add(AppConst.MSG_DESCRIPTION, "empleados agregados");
					log.info("empleados agregados");
				}else {
					status = HttpStatus.BAD_GATEWAY;
					headers.addAll(resultEmployee.getHeaders());
				}
			}else {
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_NOT_FOUND+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, AppConst.MSG_ORGANIZATION_ERROR_FOUND+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}


	public ResponseEntity<Boolean> exist(Long id) {
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Boolean result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Organization> searched = organizationRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = true;
				headers.add(AppConst.MSG_DESCRIPTION, AppConst.MSG_ORGANIZATION_FOUND);
				log.info(AppConst.MSG_ORGANIZATION_FOUND);
			}else {
				result = false;
				headers.add(AppConst.MSG_DESCRIPTION,AppConst.MSG_ORGANIZATION_NOT_FOUND+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, AppConst.MSG_ORGANIZATION_ERROR_FOUND+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	
	

}
